import moviepy.editor as mp
import speech_recognition as sr

def transcribe_video(video_path):
    # Extract audio from video
    video = mp.VideoFileClip(video_path)
    audio = video.audio.write_audiofile("audio.wav")

    # Transcribe audio to text
    r = sr.Recognizer()
    with sr.AudioFile("audio.wav") as source:
        audio = r.record(source)
        text = r.recognize_google(audio)

    return text

# Provide the path to your video file
video_path = "path/to/your/video.mp4"

# Call the transcribe_video function
transcription = transcribe_video(video_path)

# TODO WRITE THE TRANSCRIPTION IN A FILE
# Print the transcribed text
print(transcription)